<?php

	require_once 'dbconfig.php';
	
	if(isset($_GET['delete_id'])) {
		// select image from db to delete
		$stmt_select = $DB_con->prepare('SELECT img_prod FROM produtos WHERE id =:uid');
		$stmt_select->execute(array(':uid'=>$_GET['delete_id']));
		$imgRow=$stmt_select->fetch(PDO::FETCH_ASSOC);
		unlink("img_prods/".$imgRow['img_prod']);
		
		// it will delete an actual record from db
		$stmt_delete = $DB_con->prepare('DELETE FROM produtos WHERE id =:uid');
		$stmt_delete->bindParam(':uid',$_GET['delete_id']);
		$stmt_delete->execute();
		
		header("Location: dashboard.php");
	}

?>
<?php require 'views/header.php'; ?>

<div class="container">

	<div class="page-header">
    	<h1 class="h2">Todos os produtos. / <a class="btn btn-default" href="addnew.php"> <span class="glyphicon glyphicon-plus"></span> &nbsp; Adicionar Produto </a></h1> 
    </div>
    
<br />

<div class="row">
<?php
	
	$stmt = $DB_con->prepare('SELECT id, nome_prod, descr_prod, img_prod FROM produtos ORDER BY id DESC');
	$stmt->execute();
	
	if($stmt->rowCount() > 0)
	{
		while($row=$stmt->fetch(PDO::FETCH_ASSOC))
		{
			extract($row);
			?>
			<div class="col-xs-3">
				<p class="page-header"><?php echo $nome_prod."/".$descr_prod; ?></p>
				<img src="img_prods/<?php echo $row['img_prod']; ?>" class="img-rounded" width="250px" height="250px" />
				<p class="page-header">
				<span>
				<a class="btn btn-info" href="editform.php?edit_id=<?php echo $row['id']; ?>" title="click for edit" onclick="return confirm('Editar mesmo?')"><span class="glyphicon glyphicon-edit"></span> Edit</a> 
				<a class="btn btn-danger" href="?delete_id=<?php echo $row['id']; ?>" title="click for delete" onclick="return confirm('Deletar mesmo')"><span class="glyphicon glyphicon-remove-circle"></span> Delete</a>
				</span>
				</p>
			</div>       
			<?php
		}
	}
	else
	{
		?>
        <div class="col-xs-12">
        	<div class="alert alert-warning">
            	<span class="glyphicon glyphicon-info-sign"></span> &nbsp; No Data Found ...
            </div>
        </div>
        <?php
	}
	
?>
</div>	



<?php require 'views/footer.php'; ?>

</div>
