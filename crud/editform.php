<?php

	error_reporting( ~E_NOTICE );
	
	require_once 'dbconfig.php';
	
	if(isset($_GET['edit_id']) && !empty($_GET['edit_id'])) {
		$id = $_GET['edit_id'];
		$stmt_edit = $DB_con->prepare('SELECT nome_prod, descr_prod, img_prod FROM produtos WHERE id =:id');
		$stmt_edit->execute(array(':id'=>$id));
		$edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
		extract($edit_row);
	} else {
		header("Location: dashboard.php");
	}
	
	
	
	if(isset($_POST['btn_save_updates'])) {
		
		$nomeProd = $_POST['nome_prod'];// nome produto
		$descrProd = $_POST['descr_prod'];// descrição produto
		
		$imgFile = $_FILES['img_prod']['name'];
		$tmp_dir = $_FILES['img_prod']['tmp_name'];
		$imgSize = $_FILES['img_prod']['size'];
					
		if($imgFile) {
			$upload_dir = 'img_prods/'; // upload directory	
			$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
			$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
			$imgProd = rand(1000,1000000).".".$imgExt;
			
			if(in_array($imgExt, $valid_extensions)) {			
				if($imgSize < 5000000) {
					unlink($upload_dir.$edit_row['img_prod']);
					move_uploaded_file($tmp_dir,$upload_dir.$imgProd);
				} else {
					$errMSG = "Desculpe, seu arquivo é muito grande. 5mb limite";
				}
			} else {
				$errMSG = "Desculpe, apenas JPG, JPEG, PNG & GIF arquivos são permitidos.";		
			}	
		} else {
			// if no image selected the old image remain as it is.
			$imgProd = $edit_row['img_prod']; // old image from database
		}	
						
		
		// if no error occured, continue ....
		if(!isset($errMSG)) {
			$stmt = $DB_con->prepare('UPDATE produtos 
									     SET nome_prod=:nomeprod, 
										 	 descr_prod=:descrprod, 
										     img_prod=:imgprod 
								       WHERE id=:id');
			$stmt->bindParam(':nomeprod',$nomeProd);
			$stmt->bindParam(':descrprod',$descrProd);
			$stmt->bindParam(':imgprod',$imgProd);
			$stmt->bindParam(':id',$id);
				
			if($stmt->execute()){
				?>
                <script>
				alert('Atualizado com Sucesso ...');
				window.location.href='dashboard.php';
				</script>
                <?php
			}
			else {
				$errMSG = "Nenhum dado foi encontrado!";
			}
		
		}
		
						
	}
	
?>
<?php require 'views/header.php'; ?>


<div class="container">


	<div class="page-header">
    	<h1 class="h2">Atualizar Produto <a class="btn btn-default" href="dashboard.php"> Todos os Produtos </a></h1>
    </div>

<div class="clearfix"></div>

<form method="post" enctype="multipart/form-data" class="form-horizontal">
	
    
    <?php
	if(isset($errMSG)){
		?>
        <div class="alert alert-danger">
          <span class="glyphicon glyphicon-info-sign"></span> &nbsp; <?php echo $errMSG; ?>
        </div>
        <?php
	}
	?>
   
    
	<table class="table table-bordered table-responsive">
	
    <tr>
    	<td><label class="control-label">Nome do Produto.</label></td>
        <td><input class="form-control" type="text" name="nome_prod" value="<?php echo $nome_prod; ?>" required /></td>
    </tr>
    
    <tr>
    	<td><label class="control-label">Descrição do Produto.</label></td>
        <td><input class="form-control" type="text" name="descr_prod" value="<?php echo $descr_prod; ?>" required /></td>
    </tr>
    
    <tr>
    	<td><label class="control-label">Imagem do Produto.</label></td>
        <td>
        	<p><img src="img_prods/<?php echo $img_prod; ?>" height="150" width="150" /></p>
        	<input class="input-group" type="file" name="img_prod" accept="image/*" />
        </td>
    </tr>
    
    <tr>
        <td colspan="2"><button type="submit" name="btn_save_updates" class="btn btn-default">
        <span class="glyphicon glyphicon-save"></span> Atualizar
        </button>
        
        <a class="btn btn-default" href="dashboard.php"> <span class="glyphicon glyphicon-backward"></span> Cancelar </a>
        
        </td>
    </tr>
    
    </table>
    
</form>

<?php require 'views/footer.php'; ?>