<?php

	error_reporting( ~E_NOTICE ); // avoid notice
	
	require_once 'dbconfig.php';
	
	if(isset($_POST['btnsave'])) {
		$nomeProd = $_POST['nome_prod'];// nome produto
		$descrProd = $_POST['descr_prod'];// descrição produto
		
		$imgFile = $_FILES['img_prod']['name'];
		$tmp_dir = $_FILES['img_prod']['tmp_name'];
		$imgSize = $_FILES['img_prod']['size'];
		
		
		if(empty($nomeProd)){
			$errMSG = "Por favor entre com o nome do produto.";
		}
		else if(empty($descrProd)){
			$errMSG = "Por favor entre com a descrição do produto.";
		}
		else if(empty($imgFile)){
			$errMSG = "Por favor entre com a imagem do produto.";
		}
		else {
			$upload_dir = 'img_prods/'; // upload directory
	
			$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
		
			// valid image extensions
			$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
		
			// rename uploading image
			$imgProd = rand(1000,1000000).".".$imgExt;
				
			// allow valid image file formats
			if(in_array($imgExt, $valid_extensions)){			
				// Check file size '5MB'
				if($imgSize < 5000000)				{
					move_uploaded_file($tmp_dir,$upload_dir.$imgProd);
				}
				else{
					$errMSG = "Desculpe, seu arquivo é muito grande.";
				}
			}
			else{
				$errMSG = "Desculpe, apenas JPG, JPEG, PNG & GIF arquivos são permitidos.";		
			}
		}
		
		
		// if no error occured, continue ....
		if(!isset($errMSG)) {
			$stmt = $DB_con->prepare('INSERT INTO produtos(nome_prod,descr_prod,img_prod) VALUES(:nomeprod, :descrprod, :imgprod)');
			$stmt->bindParam(':nomeprod',$nomeProd);
			$stmt->bindParam(':descrprod',$descrProd);
			$stmt->bindParam(':imgprod',$imgProd);
			
			if($stmt->execute()) {
				$successMSG = "Inserido com sucesso";
				header("refresh:5;dashboard.php"); // Redireciona
			} else {
				$errMSG = "Erro ao Inserir...";
			}
		}
	}
?>
<?php require 'views/header.php'; ?>

<div class="container">


	<div class="page-header">
    	<h1 class="h2">add new user. <a class="btn btn-default" href="dashboard.php"> <span class="glyphicon glyphicon-eye-open"></span> &nbsp; Ver todos </a></h1>
    </div>
    

	<?php
	if(isset($errMSG)){
			?>
            <div class="alert alert-danger">
            	<span class="glyphicon glyphicon-info-sign"></span> <strong><?php echo $errMSG; ?></strong>
            </div>
            <?php
	}
	else if(isset($successMSG)){
		?>
        <div class="alert alert-success">
              <strong><span class="glyphicon glyphicon-info-sign"></span> <?php echo $successMSG; ?></strong>
        </div>
        <?php
	}
	?>   

<form method="post" enctype="multipart/form-data" class="form-horizontal">
	    
	<table class="table table-bordered table-responsive">
	
    <tr>
    	<td><label class="control-label">Nome do Produto.</label></td>
        <td><input class="form-control" type="text" name="nome_prod" placeholder="Nome do Produto" value="<?php echo $nomeProd; ?>" /></td>
    </tr>
    
    <tr>
    	<td><label class="control-label">Descrição do Produto.</label></td>
        <td><input class="form-control" type="text" name="descr_prod" placeholder="Descrição do Produto" value="<?php echo $descrProd; ?>" /></td>
    </tr>
    
    <tr>
    	<td><label class="control-label">Imagem do Produto.</label></td>
        <td><input class="input-group" type="file" name="img_prod" accept="image/*" /></td>
    </tr>
    
    <tr>
        <td colspan="2"><button type="submit" name="btnsave" class="btn btn-default">
       	Salvar
        </button>
        </td>
    </tr>
    
    </table>
    
</form>

</div>

<?php require 'views/footer.php'; ?>