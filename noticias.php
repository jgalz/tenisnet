<?php require 'views/header.php'; ?>

<!-- começo noticias-->
<div class="container" style="padding: 50px;">
    <div class="list-group">

        <a href="#" class="list-group-item list-group-item-action">
            <div class="d-flex justify-content-between">
                <h5>QUEIMA DE ESTOQUE</h5>
                <small>Há 1s</small>
            </div>
            <p>Produtos com ate 70% off.</p>
        </a>

        <a href="#" class="list-group-item list-group-item-action">
            <div class="d-flex justify-content-between">
                <h5>MAIS CONFIANCA PARA VOCE</h5>
                <small>Há dois minutos</small>
            </div>
            <p>Agora você pode fazer esportes radicais mais protegido.</p>
        </a>

        <a href="#" class="list-group-item list-group-item-action">
            <div class="d-flex justify-content-between">
                <h5>ESTILO</h5>
                <small>Há 35 minutos</small>
            </div>
                <p>Confira a nova linha de luvas de boxe naja first.</p>
        </a>

        <a href="#" class="list-group-item list-group-item-action">
            <div class="d-flex justify-content-between">
                <h5>Design e conforto</h5>
                <small>Há 50 minutos</small>
            </div>
                <p>Com os novos saco de dormir nautika viper.</p>
        </a>

        <a href="#" class="list-group-item list-group-item-action">
            <div class="d-flex justify-content-between">
                <h5>NOVA LINHA</h5>
                <small>Há uma hora</small>
            </div>
                <p>O novo truck blindado invert rodas gel tiger.</p>
        </a>

        <a href="#" class="list-group-item list-group-item-action">
            <div class="d-flex justify-content-between">
                <h5>WORSHOP, NAO PERCA!</h5>
                <small>Há duas horas</small>
            </div>
            <p>Este final de semana, workshop.<br/> Aberto todos os sábados, até 25 de Julho, das 14h ás 22h.</p>
        </a>

        

    </div>
</div>

<div class="container mt-5">
		<ul class="pagination pagination-lg justify-content-center">
			<li class="page-item"><a href="index.php" class="page-link">1</a></li>
			<li class="page-item active"><a href="noticias.php" class="page-link">2</a></li>
			<li class="page-item"><a href="estoques.php" class="page-link">3</a></li>
			<li class="page-item"><a href="contato.php" class="page-link">4</a></li>
		</ul>
	</div>

<?php require 'views/footer.php'; ?>