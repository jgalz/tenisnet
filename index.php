<?php require 'views/header.php';  ?>

<!-- Carousel -->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="imgs/sport1.jpg" height="550" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="imgs/sport2.jpg" height="550" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="imgs/sport3.jpg" height="550" alt="Third slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="imgs/sport4.jpg" height="550" alt="Four slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<div class="container mt-5">
    <div class="row">   
        <div class="col-lg-3">
            <div class="card">
                <img src="imgs/galeria1.jpg" class="img-fluid" alt="Responsive image"/>
            </div>
        </div>
		
		<div class="col-lg-3">
            <div class="card">
                <img src="imgs/galeria2.jpg" class="img-fluid" alt="Responsive image"/>
            </div>
        </div>
        
        <div class="col-lg-3">
            <div class="card">
                <img src="imgs/galeria3.jpg" class="img-fluid" alt="Responsive image"/>
            </div>
        </div>

		<div class="col-lg-3">
            <div class="card">
                <img src="imgs/galeria4.jpg" class="img-fluid" alt="Responsive image"/>
            </div>
        </div>
	</div>
</div>
		
	
	<div class="container mt-5">
		<ul class="pagination pagination-lg justify-content-center">
			<li class="page-item active"><a href="index.php" class="page-link">1</a></li>
			<li class="page-item"><a href="noticias.php" class="page-link">2</a></li>
			<li class="page-item"><a href="estoques.php" class="page-link">3</a></li>
			<li class="page-item"><a href="contato.php" class="page-link">4</a></li>
		</ul>
	</div>



<?php require 'views/footer.php'; ?>