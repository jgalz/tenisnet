<?php require 'views/header.php'; ?>


<!-- Login section -->
<section class="login-section">
<div class="container bg">
    <div class="row justify-content-lg-center">
        <div class="col-lg-6 mt-5">
                    <?php 
                        if(@$_GET['Empty']==true) {
                    ?>
                    <div class="alert-light text-danger text-center py-3"><?php echo $_GET['Empty'] ?></div>                             
                    <?php
                        }
                    ?>

 
                    <?php 
                        if(@$_GET['Invalid']==true)
                        {
                    ?>
                        <div class="alert-light text-danger text-center py-3"><?php echo $_GET['Invalid'] ?></div>                                
                    <?php
                        }
                    ?>
            <form action="login/process.php" method="POST">
                <div class="form-group">
                    <label for="">Username:</label>
                    <input class="form-control" type="text" name="UName">

                    <label for="">Senha:</label>
                    <input class="form-control" type="password" name="Password">

                    <button type="submit" name="Login" class="btn btn-success mt-3">Logar</button>
                 </div>
            </form>
        </div>
    </div>
</div>
</section>


<?php require 'views/footer.php'; ?>