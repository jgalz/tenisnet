   
<div class="container-fluid mt-5">
    <div class="row justify-content-center">
        <footer>
                <small class="copyright">
                    <p>Copyright © 2019 - Todos os Direitos Reservados ao <a href="index.html" title="Gabriel - Site Projeto ">Gabriel Pedlowski</a></p>
                </small><!-- fim .copyright -->
                <small class="desenvolvedor">
                    <figure>
                        <a href="index.html" title="Desenvolvido por Gabriel Pedlowski"></a>
                    </figure>
                </small><!-- fim .desenvolvedor -->
            </footer><!-- fim footer -->
        </footer><!-- fim footer -->
    </div>
</div>

   <script src="js/jquery-3.3.1.min.js"></script> 
   <script src="js/bootstrap.bundle.min.js"></script> 
   <script>
		$('.carousel').carousel({
		  interval: 5000
		}); 
	</script>
</body>
</html>