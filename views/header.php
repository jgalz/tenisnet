<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tênis Net</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<div class="">
  <div class="container">

    <nav class="navbar navbar-expand-lg navbar-light">
      <a class="navbar-brand" href="index.php">
        <img class="logo" src="imgs/net.png" alt="logo">
      </a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse menu" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="index.php">HOME</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="noticias.php">NOTÍCIAS</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="estoques.php">ESTOQUE</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="contato.php">CONTATOS</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="login.php">Adm</a>
          </li>
        </ul>
      </div>
    </nav>

  </div>
</div>

<div class="sup bordo"></div>
