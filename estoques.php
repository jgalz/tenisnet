<?php require 'views/header.php'; 

require_once 'crud/dbconfig.php';
?>


<div class="container mt-5">

    <div class="row">
<?php
	
	$stmt = $DB_con->prepare('SELECT id, nome_prod, descr_prod, img_prod FROM produtos ORDER BY id DESC');
	$stmt->execute();
	
	if($stmt->rowCount() > 0)
	{
		while($row=$stmt->fetch(PDO::FETCH_ASSOC))
		{
			extract($row);
			?>

            <div class="col-lg-3">
                <div class="card">
                    <img src="crud/img_prods/<?php echo $row['img_prod']; ?>" class="img-fluid" alt="Responsive image"/>
                    <div class="card-body">
                    <h5 class="card-title"> <?php echo $nome_prod; ?> </h5>
                    <p class="card-text"> <?php echo $descr_prod; ?> </p>
                </div>
            </div>
            
			<?php
		}
	}
	else
	{
		?>
        <div class="col-xs-12">
        	<div class="alert alert-warning">
            	<span class="glyphicon glyphicon-info-sign"></span> &nbsp; Nenhum produto cadastrado
            </div>
        </div>
        <?php
	}
	
?>
</div>	





<div class="container mt-5">
		<ul class="pagination pagination-lg justify-content-center">
			<li class="page-item"><a href="index.php" class="page-link">1</a></li>
			<li class="page-item"><a href="noticias.php" class="page-link">2</a></li>
			<li class="page-item active"><a href="estoques.php" class="page-link">3</a></li>
			<li class="page-item"><a href="contato.php" class="page-link">4</a></li>
		</ul>
	</div>


<?php require 'views/footer.php'; ?>
