<?php require 'views/header.php'; ?>


<!-- Serviços section -->
<div class="">
<section class="">
    <div class="container bg contatos">
        <div class="row justify-content-lg-center">
            <div class="col-lg-9">
                <h2 class="mt-5">Gostaria de enviar alguma sugestão ou reclamação?</h2>
                <p class="mb-5">Preencha os campos abaixo com seus dados.</p>
            </div>
        </div>
    

    <div class="row justify-content-lg-center">                  
        <div class="col-lg-6">
        <form action="">
            <div class="form-group">
                <label for="">Nome completo:</label>
                <input class="form-control" type="text" name="nome">

                <label for="">Celular - (DDD+número):</label>
                <input class="form-control" type="text" name="nome">

                <label for="">E-mail:</label>
                <input class="form-control" type="text" name="nome">

                <label for="">Mensagem</label>
                <textarea class="form-control" name="" id="" cols="10" rows="5"></textarea>

                <button class="btn btn-success mt-3">Enviar</button>
            </div>
        </form>
        </div>
    </div>
    </div>
</section>
</div>

<div class="container mt-5">
		<ul class="pagination pagination-lg justify-content-center">
			<li class="page-item"><a href="index.php" class="page-link">1</a></li>
			<li class="page-item"><a href="noticias.php" class="page-link">2</a></li>
			<li class="page-item"><a href="estoques.php" class="page-link">3</a></li>
			<li class="page-item active"><a href="contato.php" class="page-link">4</a></li>
		</ul>
	</div>


<?php require 'views/footer.php'; ?>
